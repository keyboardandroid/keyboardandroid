package com.example.keyboardandroid;

import android.content.Context;
import android.graphics.Canvas;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.view.KeyEvent;
import android.view.View;
import android.os.Vibrator;
import android.view.inputmethod.InputConnection;

                                            // Metodo de Entrada        // Interfaz   // escucha los eventos del Teclado
public class KeyboardFunctionality extends InputMethodService implements KeyboardView.OnKeyboardActionListener {
    public String texto = "";
    public KeyboardView crear_teclado;                                                              // Variable encargada de leer los eventos virtuales del Teclado
    public Keyboard teclado;                                                                        // Variable encargada de la vista del Teclado
    public emojiscodes emojis = new emojiscodes();
    public boolean mayus = false;
    ///public boolean BloqMayus = false;

    @Override
    public View onCreateInputView() {                                                               // Metodo encargado de crear la vista del Teclado
        crear_teclado = (KeyboardView)getLayoutInflater().inflate(R.layout.teclado, null);     // Asignamos a crear_teclado la vista previa del teclado
        teclado = new Keyboard(this,R.xml.qwerty);                                           // Asignamos a teclado el objeto Keyboard (almacena la vista del teclado)
                                                                                                    // this -> acceder a la clase Activity
                                                                                                    // R.xml -> acceder al reurso qwerty

        crear_teclado.setKeyboard(teclado);                                                         // Asignamos al objeto crear_tecaldo encargado de escuchar los eventos del Teclado
                                                                                                    // el objeto teclado encargado de la vista del teclado
        crear_teclado.setOnKeyboardActionListener(this);                                            // Le damos al objeto crear_teclado la posivilidad de leer y escuchar los eventos
                                                                                                    // virtuales del Teclado
        return crear_teclado;
    }

    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        char codigo;
        InputConnection ic = getCurrentInputConnection();                                           // Objeto encargado de comunicarse con el input en el cual el tecaldo actua
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(100);
        playClicks (primaryCode);
        switch (primaryCode){
            case Keyboard.KEYCODE_DELETE:
                ic.deleteSurroundingText(1,0);
                break;
            case Keyboard.KEYCODE_SHIFT:
                //if (!BloqMayus)
                    setMayus(!mayus);
                //else
                    //setMayus(false);
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            case 160:
                codigo = 'á';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 131:
                codigo = 'â';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 132:
                codigo = 'ä';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 133:
                codigo = 'à';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 134:
                codigo = 'å';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 142:
                codigo = 'Ä';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 143:
                codigo = 'Å';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 181:
                codigo = 'Á';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 182:
                codigo = 'Â';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 198:
                codigo = 'ã';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 199:
                codigo = 'Ã';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 164:
                if (!mayus)
                    codigo = 'ñ';
                else
                    codigo = 'Ñ';
                setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 130:
                codigo = 'é';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 137:
                codigo = 'ë';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 144:
                codigo = 'É';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 211:
                codigo = 'Ë';
                //if (!BloqMayus)
                //    setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 161:
                codigo = 'í';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 139:
                codigo = 'ï';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 214:
                codigo = 'Í';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 216:
                codigo = 'Ï';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 162:
                codigo = 'ó';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 148:
                codigo = 'ö';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 224:
                codigo = 'Ó';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 153:
                codigo = 'Ö';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 163:
                codigo = 'ú';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 129:
                codigo = 'ü';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 233:
                codigo = 'Ú';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 154:
                codigo = 'Ü';
                //setMayus(false);
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 256: // Corazon
                codigo = '\u2764';
                ic.commitText(String.valueOf(codigo),1);
                break;
            /*case 257:
                codigo = CARA_SONRIENTE;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 258:
                codigo = CARA_SONRIENTE_CON_DIENTES_Y_CON_OJOS_SONRIENTES;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 259:
                codigo = CARA_CON_LAGRIMAS_DE_ALEGRIA;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 260:
                codigo = CARA_SONRIENTE_CON_LA_BOCA_ABIERTA;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 261:
                codigo = CARA_SONRIENTE_CON_LA_BOCA_ABIERTA_Y_LOS_OJOS_SONRIENTES;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 262:
                codigo = CARA_SONRIENTE_CON_LA_BOCA_ABIERTA_Y_EL_SUDOR_FRIO;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 263:
                codigo = CARA_SONRIENTE_CON_LA_BOCA_ABIERTA_Y_LOS_OJOS_BIEN_CERRADOS;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 264:
                codigo = CARA_SONRIENTE_CON_HALO;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 265:
                codigo = CARA_SONRIENTE_CON_CUERNOS;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 266:
                codigo = CARA_DE_GUINIO;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 267:
                codigo = CARA_SONRIENTE_CON_OJOS_SONRIENTES;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 268:
                codigo = CARA_SABOREANDO_DELICIOSA_COMIDA;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 269:
                codigo = CARA_ALIVIADA;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 270:
                codigo = CARA_SONRIENTE_CON_OJOS_EN_FORMA_DE_CORAZON;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 271:
                codigo = CARA_SONRIENTE_CON_GAFAS_DE_SOL;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 272:
                codigo = CARA_SONRIENTE_INSINUANDO;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 273:
                codigo = CARA_NEUTRAL;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 274:
                codigo = CARA_INEXPRESIVA;
                ic.commitText(String.valueOf(codigo),1);
                break;*/
            case 275:
                codigo = emojis.CARA_TRISTE;
                texto += codigo;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 276:
                codigo = emojis.CALAVERA;
                texto += codigo;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 277:
                codigo = emojis.CORAZON_ROJO;
                texto += codigo;
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 278:
                codigo = '@';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 279:
                codigo = '$';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 280:
                codigo = '&';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 281:
                codigo = '*';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 282:
                codigo = '+';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 283:
                codigo = '<';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 284:
                codigo = '>';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 285:
                codigo = '=';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 286:
                codigo = '?';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 287:
                codigo = '|';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 288:
                codigo = '!';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 289:
                codigo = '&';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 290:
                codigo = '#';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 291:
                codigo = '·';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 292:
                codigo = '%';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 293:
                codigo = '\'';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 294:
                codigo = '(';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 295:
                codigo = ')';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 296:
                codigo = '/';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 297:
                codigo = '\\';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 298:
                codigo = '¡';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 299:
                codigo = '¿';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 300:
                codigo = '[';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 301:
                codigo = ']';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 302:
                codigo = '{';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 303:
                codigo = '}';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 304:
                codigo = '^';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 305:
                codigo = '\u2642';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 306:
                codigo = '\u2640';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 307:
                codigo = '\u2695';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 308:
                codigo = '\u2696';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 309:
                codigo = '\u2708';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 310:
                codigo = '\u2B1B';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 311:
                codigo = '\u2744';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 312:
                codigo = '\u2618';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 313:
                codigo = '\u2615';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 314:
                codigo = '\u26F0';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 315:
                codigo = '\u26EA';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 316:
                codigo = '\u26E9';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 317:
                codigo = '\u26F2';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 318:
                codigo = '\u26FA';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 319:
                codigo = '\u2668';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 320:
                codigo = '\u26FD';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 321:
                codigo = '\u2693';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 322:
                codigo = '\u26F5';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 323:
                codigo = '\u26F4';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 324:
                codigo = '\u231B';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 325:
                codigo = '\u23F2';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 326:
                codigo = '\u2600';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 327:
                codigo = '\u2601';
                ic.commitText(String.valueOf(codigo),1);
                break;
            case 328:
                codigo = '\u26A1';
                ic.commitText(String.valueOf(codigo),1);
                break;
            default:
                //if (!BloqMayus)
                //    setMayus(false);
                codigo = (char) primaryCode;
                if(Character.isLetter(codigo) && mayus)
                    codigo = Character.toUpperCase(codigo);
                ic.commitText(String.valueOf(codigo),1);
        }

    }

    //@Override
    //public boolean onKeyLongPress( int keyCode, KeyEvent event ) {
    //    if (keyCode == Keyboard.KEYCODE_SHIFT)         // Si la tecla pulsada es el SHIFT
    //        if (BloqMayus = true)                      // asigno Bloqmayus a true y el resultado de la asignación la evaluo en el if
    //            return true;                           // Si el resultado es positivo retorno true
    //    return false;                                  // Si no, retorno false
    //}

    private void setMayus (boolean estado) {        // Funcion para fijar las mayusculas (true) o minusculas (false)
        mayus = estado;                             // Fijo la variable mayus al valor de estado
        teclado.setShifted(mayus);                  // Cambio el estado del teclado a mayusculas
        crear_teclado.invalidateAllKeys();          // Aplico el cambio
    }

    private void playClicks(int primaryCode) {
        AudioManager audio = (AudioManager)getSystemService(AUDIO_SERVICE);
        switch (primaryCode) {
            case 32:
                audio.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
            case Keyboard.KEYCODE_DONE:
                audio.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                audio.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default:
            audio.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
