package com.example.keyboardandroid;

public class emojiscodes {
    /* Emojis */
    public char CARA_SONRIENTE = '\uF600'; // &#128512;
    public char CARA_SONRIENTE_CON_DIENTES_Y_CON_OJOS_SONRIENTES = '\uF601'; // &#128513
    private char CARA_CON_LAGRIMAS_DE_ALEGRIA = '\uF602'; // &#128514
    private char CARA_SONRIENTE_CON_LA_BOCA_ABIERTA = '\uF603'; // &#128515
    private char CARA_SONRIENTE_CON_LA_BOCA_ABIERTA_Y_LOS_OJOS_SONRIENTES = '\uF604'; // &#128516
    private char CARA_SONRIENTE_CON_LA_BOCA_ABIERTA_Y_EL_SUDOR_FRIO = '\uF605'; // &#128517
    private char CARA_SONRIENTE_CON_LA_BOCA_ABIERTA_Y_LOS_OJOS_BIEN_CERRADOS = '\uF606'; // &#128518
    private char CARA_SONRIENTE_CON_HALO = '\uF607'; // &#128519
    private char CARA_SONRIENTE_CON_CUERNOS = '\uF608'; // &#128520
    private char CARA_DE_GUINIO = '\uF609'; // &#128521
    private char CARA_SONRIENTE_CON_OJOS_SONRIENTES = '\uF60A'; // &#128522
    private char CARA_SABOREANDO_DELICIOSA_COMIDA = '\uF60B'; // &#128523
    private char CARA_ALIVIADA = '\uF60C'; // &#128524
    private char CARA_SONRIENTE_CON_OJOS_EN_FORMA_DE_CORAZON = '\uF60D'; // &#128525
    private char CARA_SONRIENTE_CON_GAFAS_DE_SOL = '\uF60E'; // &#128526
    private char CARA_SONRIENTE_INSINUANDO = '\uF60E'; //&#128527
    private char CARA_NEUTRAL = '\uF610'; // &#128528
    private char CARA_INEXPRESIVA = '\uF611'; // &#128529
    public char CARA_TRISTE = '\u2639'; // &#9785;
    public char CALAVERA = '\u2620'; // &#9760;
    public char CORAZON_ROJO = '\u2763'; // &#10083;
}