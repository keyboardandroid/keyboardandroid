package com.example.keyboardandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import yuku.ambilwarna.AmbilWarnaDialog;

import static android.graphics.Color.*;

public class MainActivity extends AppCompatActivity {

    int colorDefecto;
    Button botonColorFondoTeclado;
    KeyboardView teclado;
    private Window window;

    public MainActivity() throws ParserConfigurationException, IOException, SAXException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.window = getWindow();

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             String primero = "#00701a";
             String segundo = "#43a047";
             String tercero = "#494949";

             CambiarColor(primero, segundo, tercero);
         }
     });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick (View v){
            String primero = "#5c007a";
            String segundo = "#8e24aa";
            String tercero = "#757575";

            CambiarColor(primero, segundo, tercero);
        }
    });
        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener(){
        @Override
        public void onClick(View v){
            String primero = "#ab000d";
            String segundo = "#e53935";
            String tercero = "#546e7a";

        CambiarColor(primero, segundo, tercero);
        }
        });

        // Diego con su color picker
        colorDefecto = ContextCompat.getColor(MainActivity.this, R.color.colorPrimary);
        botonColorFondoTeclado = (Button) findViewById(R.id.button_bgkbdcolor);
        botonColorFondoTeclado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirColorPicker();
            }
        });
    }

    public void abrirColorPicker () {
        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, colorDefecto, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {

            }

            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                colorDefecto = color;
                teclado.setBackgroundColor(color);
            }
        });

        colorPicker.show();
    }

    // Metodo que Detecta las Pulsaciones del usuario en Pantalla
    public void pulsaciones(View pulsado) {
        Toast.makeText(this, R.string.mensaje, Toast.LENGTH_SHORT).show();
        /* Toast --> Objeto de tipo evento
           .makeText --> Metodo encargado de mostrar un mensaje, como parametros recibe:
                - this --> Hace referencia al Fichero activity_main
                - R.string.mensaje --> Mensaje a mostrar, definido en el fichero strings
                - Toas.LENGHT_SHORT --> Tiempo del mensaje mostrandose en pantalla
           .show() --> Metodo encargado de mostar el evento en pantalla
        */
    }
    private void CambiarColor(String primero, String segundo, String tercero) {
        //colorPrimaryDark
        getResources().getColor(R.color.primero);
        //colorPrimary
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(parseColor(primero)));
        //bg
        window.setBackgroundDrawable(new ColorDrawable(parseColor(segundo)));
        //bottom navigation
        window.setNavigationBarColor(parseColor(tercero));
    }
        /*
        ESTO ES PARA CAMBIAR COLOR DEL TECLADO LLAMANDO A LOS BOTONES --> NO FUNCIONA
    DocumentBuilderFactory colores = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = colores.newDocumentBuilder();
    Document boton = builder.parse(new File("app/res/drawable/normal.xml"));

    NodeList items = boton.getElementsByTagName("color1");

         */
}
