package com.example.keyboardandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Metodo que Detecta las Pulsaciones del usuario en Pantalla
    public void pulsaciones(View pulsado) {
        Toast.makeText(this, R.string.mensaje, Toast.LENGTH_SHORT).show();
        /* Toast --> Objeto de tipo evento
           .makeText --> Metodo encargado de mostrar un mensaje, como parametros recibe:
                - this --> Hace referencia al Fichero activity_main
                - R.string.mensaje --> Mensaje a mostrar, definido en el fichero strings
                - Toas.LENGHT_SHORT --> Tiempo del mensaje mostrandose en pantalla
           .show() --> Metodo encargado de mostar el evento en pantalla
        */
    }
}
