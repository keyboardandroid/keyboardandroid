# Sprint 1
## Historias  
- Investigación  
    - Arbol de Directorios de Android Studio  
    - Funcionamiento y sintaxis del Lenguaje Java  
    - Sintaxis del Lenguaje de Marcas XML  
- Reconocimiento de la APP como teclado para Android  
- Reconocer las Pulsaciones del usuario en Pantalla  
- Desarrollo del contorno de las teclas  
- Desarrollo de Funcionalidad de Teclas (leer el caracter especificado)  

## Planning Poker
Magnitud --> Dias  
- Investigación --> Tiempo estimado 5  
- Reconocimiento de la APP como teclado para Android --> Tiempo estimado 3  
- Reconocer las Pulsaciones del usuario en Pantalla --> Tiempo estimado 4  
- Desarrollo del contorno de las teclas --> Tiempo estimado 3  
- Desarrollo de Funcionalidad de Teclas (leer el caracter especificado) --> Tiempo estimado 3  

## Asignación de Tareas
- Reconocimiento de la APP como teclado para Android --> Diego Martin  
- Reconocer las Pulsaciones del usuario en Pantalla --> Ovidiu Aricsan  
- Desarrollo del contorno de las teclas --> Victor Guerra  
- Desarrollo de Funcionalidad de Teclas (leer el caracter especificado) --> Daniel Navarro  
