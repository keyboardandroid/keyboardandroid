# Estructura de directorios en un proyecto con Android Studio
- https://www.movilzona.es/tutoriales/android/desarrollo/curso-de-desarrollo-android-tema-7-estructura-interna-de-una-aplicacion-android/  
- http://www.sgoliver.net/blog/estructura-de-un-proyecto-android-android-studio/  
- https://www.movilzona.es/tutoriales/android/desarrollo/curso-de-desarrollo-android-tema-7-estructura-interna-de-una-aplicacion-android/  
- http://www.sgoliver.net/blog/estructura-de-un-proyecto-android-android-studio/ 

# Eventos de teclas de Android 
- https://developer.android.com/reference/android/inputmethodservice/Keyboard.Key
- https://stackoverflow.com/questions/15789997/how-to-change-background-color-of-key-for-androidsoft-keyboard

# Clases en JAVA para Android
- https://developer.android.com/reference/android/inputmethodservice/InputMethodService

# Metodos de Entrada para las Teclas
- https://developer.android.com/training/keyboard-input/style?hl=es  
- https://developer.android.com/guide/topics/text/creating-input-method?hl=es-419#java  

# Color Picker
- https://medium.com/@skydoves/how-to-implement-color-picker-in-android-61d8be348683

# Color de fondo
- https://developer.android.com/reference/android/inputmethodservice/KeyboardView

# Color de teclas individual
- https://stackoverflow.com/questions/18224520/how-to-set-different-background-of-keys-for-android-custom-keyboard

# No se puede hacer el teclado deslizable
- https://stackoverflow.com/questions/17000937/how-to-make-swipe-keyboard-like-app